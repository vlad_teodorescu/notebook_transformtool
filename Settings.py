import os.path;
import re;

import functions;

class Settings:
    def __init__ (self, file_name):
        
        if not isinstance(file_name, basestring):
            raise TypeError("File name must be a string");
        if not len(file_name):
            raise ValueError("File name is empty");
        
        self._file_name = file_name;
        self.clear();                  #also used to initialize the _settings variable
        self.init_settings();

    def init_settings(self):
        f = open(self._file_name, "r");
        settings_str = f.read();
        if not len(settings_str):
            raise IOError();
        f.close();
        
        settings_lines = settings_str.split('\n');
        
        for line in settings_lines:
            line = line.strip();
            if not len(line):
                continue;
            line = line.split('#');
            line = line[0].strip();
            if not len(line):
                continue;
            
            line = line.split('=');
            if len(line)!=2:
                self.clear();
                raise Exception("Bad settings file format");
            
            line[0] = line[0].strip();
            line[1] = line[1].strip();
            
            if not len(line[0]) or not len(line[1]):
                self.clear();
                raise Exception("Bad settings file format");
            
            try:
                self._settings[line[0]] = int(line[1]);
            except:
                self.clear();
                raise Exception("Bad setting value for "+ line[0]);
        
    
    def get(self, setting):
        #Python automatically throws
        return self._settings[setting];
    
    def set(self, setting, value):
        self._settings[setting] = int(value);
    
    def _print(self):
        for sett in self._settings.keys():
            print sett + " = " + str(self._settings[sett]);
    
    def keys(self):
        return self._settings.keys();
    
    def save(self):
        f = open(self._file_name, "r");
        settings_str = f.read();
        if not len(settings_str):
            raise IOError();
        f.close();
        
        for key in self._settings.keys():
            (settings_str, num_subs) = re.subn( '^(\s*)' + re.escape(key) + '(\s*=\s*)(\d+)(.*)$',
                                                '\g<1>' + key + '\g<2>'+str(self.get(key))+'\g<4>',
                                                settings_str, 0, re.MULTILINE);
            if (num_subs!=1):
                if num_subs<1:
                    raise Exception("Setting not found: "+key);
                if num_subs>1:
                    raise Exception("More than one value found for setting: "+key);
        
        f = open(self._file_name, "w");
        f.write(settings_str);
        f.close();
    
    
    def clear(self):
        self._settings = {};
