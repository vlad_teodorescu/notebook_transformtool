import sys;

from Tkinter import *;
from MainApp import *;

sys.path.append(os.getcwd());
	
# ----------------------------------------------- Here starts the main loop
root = Tk()
root.title('.notebook Tools');
root.wm_iconbitmap('app_icon.ico')
app = MainApp(root)
root.mainloop();
