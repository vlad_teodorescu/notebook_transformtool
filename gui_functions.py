import sys;
import os.path;
from Tkinter import *;
import tkFileDialog;
import Tkconstants;
from ScrolledText import *;
from idlelib.WidgetRedirector import WidgetRedirector

#Taken from a post on unpythonic
class ReadOnlyEntry(Entry):
	def __init__(self, *args, **kwargs):
		Entry.__init__(self, *args, **kwargs)
		self.config(insertofftime=1000)
		self.config(insertontime=0)
		self.redirector = WidgetRedirector(self)
		self.insert = self.redirector.register("insert", lambda *args, **kw: "break")
		self.delete = self.redirector.register("delete", lambda *args, **kw: "break")
		
class ReadOnlyScrolledText(ScrolledText):
	def __init__(self, *args, **kwargs):
		ScrolledText.__init__(self, *args, **kwargs)
		self.config(insertofftime=1000)
		self.config(insertontime=0)
		self.redirector = WidgetRedirector(self)
		self.insert = self.redirector.register("insert", lambda *args, **kw: "break")
		self.delete = self.redirector.register("delete", lambda *args, **kw: "break")

#Frame-derived widget which cotains a frame and inside a text box and a browse button
class OsPath(Frame):
		def __init__(self, master=None, cnf={}, **kw):
			if 'label' in kw.keys():
				self._label = kw['label'];
				del kw['label'];
			
			if 'initial_directory' in kw.keys():
				self._init_dir = kw['initial_directory'];
				del kw['initial_directory'];
			else:
				self._init_dir = StringVar();
				self._init_dir.set('C:/');
			
			if 'file_types' in kw.keys():
				self._file_types = kw['file_types'];
				del kw['file_types'];
			else:
				self._file_types = [];
			
			if 'state' in kw.keys():
				self._state = kw['state'];
				del kw['state'];
			else:
				self._state = NORMAL;
			
			if 'variable' in kw.keys():
				self._path = kw['variable'];
				del kw['variable'];
			else:
				self._path= StringVar();
				self._path.set('');
			
			
			Frame.__init__(self, master, cnf, **kw);
			
			
			
			if self._label:
				self.label_widget = Label(self, text = self._label, anchor = 'w');
				self.label_widget.grid(row = 0, column = 0, sticky = W, columnspan = 2);
				
			self.text_widget = ReadOnlyEntry(self, textvariable = self._path, state = self._state, cursor="arrow", exportselection=False, width=60)
			self.text_widget.bind("<Button-1>", self.open_dialog);
			self.browse_button = Button(self, text="Browse", state = self._state, command = self.open_dialog);
			self.text_widget.grid(row = 1, column = 0, sticky = W);
			self.browse_button.grid(row = 1, column = 1, padx = 10, sticky = W);
		
		def open_dialog(self, e1=None):
			if self._state == DISABLED:
				return;
			options = {};
			#options['defaultextension'] = '.notebook';
			options['filetypes'] = [('All files', '.*')] + self._file_types;
			options['initialdir'] = self._init_dir.get();
			
			options['parent'] = self;
			options['title'] = 'Open a notebook file';
			#f being the open file
			f = tkFileDialog.askopenfile(mode='r', **options);
			if f:
				self.setPath(os.path.abspath(f.name)); #absolute filename, e.g. d:\work\quizzes\quiz1\Chapter1.notebook
				f.close();
		
		def enable(self):
			self._state = NORMAL;
			self.__change_state();
		
		def disable(self):
			self._state = DISABLED;
			self.__change_state();
		
		def toggle(self):
			if self._state == DISABLED:
				self.enable();
			else:
				self.disable();
		
		def __change_state(self):
			self.text_widget.config(state=self._state);
			self.browse_button.config(state=self._state);
		
		#returns the current path, as a string
		def getPath(self):
			return self._path.get();
		
		#sets the current path
		def setPath(self, path):
			self._path.set(path);
			self._init_dir.set(os.path.dirname(self._path.get()));
			return True;
