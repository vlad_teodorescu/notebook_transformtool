import zipfile;
import os.path;
import re;
import shutil;
import xml.dom.minidom;
from subprocess import call;

from Tkinter import *;
import tkFileDialog;
import tkMessageBox;
import Tkconstants;
from ScrolledText import *;
import ttk;


from functions import *;
from gui_functions import *;
from Settings import *;
from AbootWindow import *;
from Archive import *;

#Constants that determine the type of message
SUCCESS = 0;
WARNING = 1;
ERROR = 2;


#Main Application class
class MainApp:

	def __init__(self, master):
		if isinstance(master, Tk):
			self._root = master;
			self.add_top_menu();
		else:
			tkMessageBox.showerror("Fatal error", "Root window not passed to constructor")
			sys.exit(1);
		
		try:
			self._settings = Settings("./settings.cfg");
			
		except IOError:
			tkMessageBox.showerror("Fatal error", "Could not open settings file");
			sys.exit(1);		
		except Exception as e:
			tkMessageBox.showerror("Fatal error", e.message);
			sys.exit(1);
		except:
			tkMessageBox.showerror("Fatal error", "Internal error");
			sys.exit(1);
			
		main_frame = Frame(master);
		main_frame.grid(sticky = NW);
		
		tabbed_container = ttk.Notebook(main_frame);
		tabbed_container.grid(sticky = NW+E, row = 0, column = 0);
		tabbed_container.add(self.__quiz_frame(), text = "Quiz Tools");
		tabbed_container.add(self.__answer_frame(), text = "Answer Tools");
		
		self._status = ReadOnlyScrolledText(main_frame, height = 10, width = 90);
		self._status.grid(row = 1, column = 0, sticky = SW); 
		self._status.tag_config("info", foreground = "black");
		self._status.tag_config("success", foreground = "white", background = "green");
		self._status.tag_config("warning", foreground = "white", background = "orange");
		self._status.tag_config("error", foreground = "white", background = "red");
	
	def __quiz_frame(self):	
		quiz_frame = Frame();
		quiz_frame.grid(sticky = W, padx = 10, pady = 10);
		
		self._initial_directory = StringVar();		#initial directory an openfile dialog is supposed to point to 
		self._initial_directory.set('c:/');
		
		self._file_path = StringVar();			#path to the .notebook file
		self._file_path.set('');
		
		self._answer_path = StringVar();		#path to the answers file
		self._answer_path.set('');
		
		self._control_answer = IntVar();		#bool controlling whether to set answers or not
		self._control_answer.set(0);
		
		self._control_align= IntVar();			#bool controlling whether to align questions and answers to grid
		self._control_align.set(0);
		
		self._control_group= IntVar();			#bool controlling whether to add question groups to the .manifest file
		self._control_group.set(0);				#EXPERMIENTAL
		
		#path of file to be worked on
		notebook_path = OsPath(quiz_frame, label='Choose .notebook file', initial_directory = self._initial_directory, file_types = [('Notebook files', '.notebook')], 
								variable = self._file_path);
		notebook_path.grid(row = 0, column = 1, sticky = SW);
		
		#set answers?
		answer_path = OsPath(quiz_frame, label = 'Choose answer file', state=NORMAL, initial_directory = self._initial_directory, variable = self._answer_path);
		answer_path.grid(row = 1, column = 1, sticky = SW);
		control_answer_path = Checkbutton(quiz_frame, command = answer_path.toggle, variable = self._control_answer);
		control_answer_path.grid(row = 1, column = 0, sticky = SW);
		control_answer_path.select();
		
		control_align = Checkbutton(quiz_frame, variable = self._control_align);
		control_align.grid(row = 2, column = 0, sticky = SW);
		control_align.select();
		text_align = Label(quiz_frame, text = 'Align questions and answers').grid(row = 2, column = 1, sticky = SW);

		control_group = Checkbutton(quiz_frame, variable = self._control_group);
		control_group.grid(row = 3, column = 0, sticky = SW);
		text_align = Label(quiz_frame, text = 'Add question groups (EXPERIMENTAL)').grid(row = 3, column = 1, sticky = SW);
		
		button = Button(quiz_frame, text="Process file", command=self.__process_file)
		button.grid(row = 4, column = 1, sticky = SW)
		
		button_processfont = Button(quiz_frame, text="Process font size", command=self.__process_font);
		button_processfont.grid(row = 4, column = 2, padx = 70, sticky = SW);
		
		
		quiz_frame.grid_rowconfigure(5,pad = 50);
		quiz_frame.grid_columnconfigure(0,weight = 1);
		quiz_frame.grid_columnconfigure(1,weight = 10);
		
		for i in range(0,5):
			quiz_frame.grid_rowconfigure(i,pad = 10);
		
		return quiz_frame;
	
	def __answer_frame(self):
		answer_frame = Frame();
		
		self._num_answers_sheet = StringVar();
		self._num_answers_sheet.set('');
		
		#An entry for the number of answers that we want generated
		num_answers_sheet = Entry(answer_frame, textvariable = self._num_answers_sheet, width=6);
		num_answers_sheet.grid(row = 0, column = 0, sticky = S, padx = 10);
		
		num_answers_button = Button(answer_frame, command=self.__generate_answer_sheet, text="Generate answers file");
		num_answers_button.grid(row = 0, column = 1, sticky = SE, padx = 10)
		
		
		return answer_frame;
	
	def add_status_line(self, text, type = None):
		if type is not None:
			if type == SUCCESS:
				prn_text = 'SUCCESS: ' + text;
				tag = ("success");
			elif type == WARNING:
				prn_text = 'WARNING: ' + text;
				tag = ("warning");
			elif type == ERROR:
				prn_text = 'ERROR: ' + text;
				tag = ("error");
			else:
				raise Exception("Unknown status type");
		else:
			prn_text = text;
			tag = ("info");
		
		self._status.insert(INSERT, text[:90] + "\n", tag);
		self._status.yview(END);
		self._status.update_idletasks();
		print prn_text;
	
	def clear_status(self):
		self._status.delete("1.0", END);
	
	#add top menus to the application
	def add_top_menu(self):
		# create a popup menu
		menubar = Menu(self._root)
		
		filemenu = Menu(menubar, tearoff = 0)
		#filemenu.add_command(label="Settings", command=hello)
		filemenu.add_separator()
		filemenu.add_command(label="Exit", command=self._root.quit)
		menubar.add_cascade(label="File", menu=filemenu)
		
		menubar.add_command(label="Aboot", command=self.aboot_window)
		# display the menu
		self._root.config(menu=menubar)
	
	def aboot_window(self):
		AbootWindow(self._root);
		pass;
	
	#most important function, launches execution of the program
	def __process_file(self):
		self.clear_status();
		self.add_status_line("Processing file \"" + self._file_path.get() + "\"");
		
		if not len(self._file_path.get()):
			self.add_status_line("No file selected", ERROR);
			return;
		
		try:
			(abs_filename, file_name, working_dir) = get_file_path(self._file_path.get());
		except IOError:
			self.add_status_line("File \""+self._file_path+"\" does not exist", ERROR);
			return ;
	
		temp_dir = working_dir+"/temp";	#temporary directory in which files are extracted

		#extract the notebook files to a temp directory. Exists on error
		try:
			extract_notebook_archive(abs_filename, temp_dir);
		except zipfile.BadZipfile: 
			self.add_status_line(file_name+" is not a proper .notebook file", ERROR);
			return;
		except zipfile.LargeZipFile:
			self.add_status_line(file_name+" is too large and requires ZIP64 functionality", ERROR);
			return;

		file_list = os.listdir(temp_dir);#list the files in the directory
		regex = re.compile('page\d+\.svg');
		
		#align question positions?
		if self._control_align.get():
			for i in file_list:
				if (i=="." or i==".." or os.path.isdir(temp_dir+"/"+i)):
					continue;
				status = None;
				if regex.match(i):
					s = i.ljust(12,' ')+": ";
					try:
						process_file(temp_dir+"/"+i, self._settings);
						s+="SUCCESS";
					except Exception as e:
						s+="FAILIURE("+e.message+")";
						status = WARNING;
						
					self.add_status_line(s, status);

					
		#set question answers?
		if self._control_answer.get():
			answer_file = self._answer_path.get();
			
			try:
				f = open(answer_file);
				all_answers = f.read();
				f.close();
				for i in file_list:
					if (i=="." or i==".." or os.path.isdir(temp_dir+"/"+i)):
						continue;
					status = None;
					if regex.match(i):
						s = i.ljust(12,' ')+": ";
						try:
							set_answers(temp_dir+"/"+i, all_answers);
							s+="SUCCESS";
						except Exception as e:
							s+="FAILIURE("+e.message+")";
							status = WARNING;
							
						self.add_status_line(s, status);
						
			except IOError:
				self.add_status_line("Answers file list not found", ERROR);
				shutil.rmtree(temp_dir);
				return;
					
		#add question groups to the imsmanifest.xml file?
		if self._control_group.get():
			status = None;
			s = "Adding question groups: ";
			try:
				mod_manifest(temp_dir);
				s+="SUCCESS";
			except Exception as e:
				s+="FAILIURE("+e.message+")";
				status = WARNING;
			
			self.add_status_line(s, status);

		try:
			compress_as_notebook_archive(abs_filename, temp_dir);
		except OSError:
			self.add_status_line("Error removing original .notebook file");
			shutil.rmtree(temp_dir, ERROR);
			return;
		except zipfile.BadZipfile:
			self.add_status_line("Unable to write back to .notebook file");
			shutil.rmtree(temp_dir, ERROR);
			return;
			
		self.add_status_line("All jobs completed succesfuly", SUCCESS);

	def __generate_answer_sheet(self):
		self.clear_status();
		self.add_status_line("Generating answer sheet");
		try:
			num_answers = int(self._num_answers_sheet.get());
			if (num_answers<1):
				raise ValueError();
		except ValueError:
			self.add_status_line("Incorrect number of answers", ERROR);
			return;
		
		#here we generate the answers. First, a save file dialog is in order
		options = {};
		options['filetypes'] = [('All files', '.*')] + [('Text files', '.txt')];
		options['initialdir'] = self._initial_directory.get();
		
		options['parent'] = self._root;
		options['title'] = 'Save answers file';
		#f being the open file
		f = tkFileDialog.asksaveasfilename(**options);
		
		try:
			generate_answer_sheet(f, num_answers);
			status = "Answers file succesfuly generated";
			s_index = SUCCESS;
		except ValueError:
			status = "Unacceptable number of answers";
			s_index = ERROR;
		except:
			status = "Could not write answers file";
			s_index = ERROR;
		self.add_status_line(status, s_index);
		
	def __process_font(self):
		
		exec_path= get_current_path() + "/setfont.exe";
		call([exec_path, "setfont.exe"])
