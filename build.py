from distutils.core import setup
import py2exe;
import os.path;
import shutil;

from functions import *;

SRC_DIR = get_current_path() + "/";

TARGET_DIR = SRC_DIR + "dist/";

setup(windows=[{
					"script":"main.py",
					"icon_resources":[(1,"app_icon.ico")]
				}])

files = ['settings.cfg', 'setfont.exe', 'LICENSE.txt', 'Readme.txt', 'app_icon.ico']
for fname in files:
    print "Copying "+fname;
    shutil.copy(SRC_DIR + fname, TARGET_DIR + fname);

print "Copying resources folder";
shutil.rmtree(TARGET_DIR + "res", ignore_errors = True);
shutil.copytree(SRC_DIR+"res", TARGET_DIR + "res");

print "\n\nBuild completed succesfuly";