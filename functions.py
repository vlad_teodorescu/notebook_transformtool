import sys;
import zipfile;
import os.path;
import re;
import shutil;
import xml.dom.minidom;


from Settings import *;

#mods a file, changing question paramteres	
def process_file(file_name, settings):
	ok = 0;
	try:
		f = open(file_name, "r");
	except IOError:
		raise Exception('Could not open file');
	
	dom = xml.dom.minidom.parse(f);
	#contents = f.read();
	#if not len(contents):
	#	return 1001;
	f.close();
	
	container = dom.getElementsByTagName("g")[0];
	items = [e for e in container.childNodes if e.nodeType == e.ELEMENT_NODE and e.tagName == "g"];
	if (len(items)<1): #not enough items, wtf
		raise Exception("File structure is invalid");
	
	for i in range(0, len(items)):
		g = items[i];
		if (g.getAttribute("class").lower() not in ['question','questionchoice']):
			raise Exception("File structure is invalid");
		
		t = g.getElementsByTagName("text"); # we get the elements tagged text, since they're the important ones
		if not i: #question
			x1 = settings.get('q_index_xpos');#45;
			x2 = settings.get('q_text_xpos');#91;
			y = settings.get('q_ypos');#128;
		else: #answer
			x1 = settings.get('a_index_xpos');#81 + 10;
			x2 = settings.get('a_text_xpos');#119 + 10;
			y = settings.get('a_ypos') + settings.get('a_spacing') * i;#170 + 74*i;
		
		if (t.length<2): #should be: index + text
			ok+=1;
			continue;
		
		#translate(81,244) rotate(0.000,15.000,10.872) scale(1.000,1.000)
		t1 = t.item(0).getAttribute("transform");
		t1 = re.sub('^\s*translate\(\d+,\d+\)\s*(.+)\s*$','translate(%d,%d) \g<1>' % (x1,y), t1);
		t.item(0).setAttribute("transform", t1);
		
		t2 = t.item(1).getAttribute("transform");
		t2 = re.sub('^\s*translate\(\d+,\d+\)\s*(.+)\s*$','translate(%d,%d) \g<1>' % (x2,y), t2);
		t.item(1).setAttribute("transform", t2);
	
	contents = dom.toprettyxml(indent='', newl='');
	
	(contents, num_replacements) = re.subn( 'scale\s*\([^\)]*\)', 'scale(1.000,1.000)',	contents);	
	
	if not ok:#only on pages where replacements have been made
		(contents, num_replacements) = re.subn( 'font-weight="bold"', '',	contents);
	
	try:
		f = open(file_name, "w");
		f.truncate(0);
		f.write(contents);
		f.close();
	except IOError:
		raise Exception("Could not write back to file");
	
#function sets answers from a file
def set_answers(file_name, all_answers):
	#ok = 0;
	if not len(all_answers):
		raise Exception("Answer file is empty");
	
	try:
		f = open(file_name, "r");
	except IOError:
		raise Exception("Could not open file");
	contents = f.read();
	if not len(contents):
		raise Exception("File is empty");
	f.close();
	
	regexp_text = "</votemetadata>[\s]*<text[^>]*>[\s]*<tspan[^>]*>[\s]*<tspan[^>]*>[\s]*<tspan[^>]*>[\s]*(\d+)[\s]*</tspan>[\s]*</tspan>";
	result = re.search(regexp_text, contents);
	
	if result is None:#not found
		raise Exception("Invalid file format");
	qid = int(result.group(1));
	if qid<1:
		raise Exception("Wrong question ID");
	
	result = re.search(str(qid)+"\s*\)\s*([a-zA-Z])", all_answers);	
	if result is None:#not found
		raise Exception("Invalid answers format");
	ans = result.group(1).upper();
	ans = ord(ans)-65+1;#65 = ASCII code of A
	if (ans<1 or ans>27):
		raise Exception("Invalid answer index");
	(contents, num_replacements) = re.subn(	'<g([^>]*)class="question"([^>]*)>\s*<votemetadata>\s*<questiontext([^>]*)correct="\d*"([^>]*)points="\d*"([^/]*)/>',
											'<g\g<1>class="question"\g<2>><votemetadata><questiontext\g<3>correct="'+str(ans)+'"\g<4>points="1"\g<5>/>',
											contents);
	if num_replacements!=1:
		raise Exception("Answer was set "+num_replacements+" times. Expected 1");
		
	try:
		f = open(file_name, "w");
		f.truncate(0);
		f.write(contents);
		f.close();
	except IOError:
		raise Exception("Could not write back file");

# This function modifies the manifest file, adding question groups and the respective title pages.
def mod_manifest(temp_dir):
	ok = 0;
	manifest_file = temp_dir+"/imsmanifest.xml";
	try:
		f = open(manifest_file, "r");
	except IOError:
		raise Exception("Could not open manifest file");
	dom = xml.dom.minidom.parse(f);
	f.close();
	
	org = dom.getElementsByTagName("organizations")[0];
	org = org.getElementsByTagName("organization")[0]; #first organization
	items = org.getElementsByTagName("item");
	
	if (items.length>1): #already more than 1 group
		raise Exception("Already more than 1 group in file");
	
	items = items.item(0);
	
	items.getElementsByTagName("title")[0].firstChild.data = "Question Bank";
	
	id_test_bank = items.getAttribute("id");
	id_test_bank = int(id_test_bank.replace("group",""));
	
	for i in range(1,4):
		qb_id = id_test_bank + i;
		aux = items.cloneNode(1);
		aux.getElementsByTagName("title")[0].firstChild.data = "Quiz "+str(i);
		aux.setAttribute("id", "group"+str(qb_id));
		aux.setAttribute("identifierref", "group"+str(qb_id)+"_pages");
		org.appendChild(aux);
	
	res = dom.getElementsByTagName("resources")[0].getElementsByTagName("resource");
	pages_resource = None;
	for i in range(0,res.length):
		if res.item(i).getAttribute("identifier") == "pages":
			pages_resource = res.item(i);
			break;
	#oops, found no page resource.
	if (pages_resource is None):
		raise Exception("Found no page resource");
	
	#iterate through the pages resource and find out the maximum page number
	pages = pages_resource.getElementsByTagName("file");
	max_id = 0;
	for i in range(0,pages.length):
		aux = pages.item(i);
		aux = int(aux.getAttribute("href").replace("page","").replace(".svg",""));
		if (aux>max_id):
			max_id = aux;
		
	#we've got the maximum page id, now copy the templates to the folder and add the nodes to the XML file
	for i in range(1,7):
		page_name = "page"+str(max_id + i)+".svg";
		shutil.copy("res/p"+str(i)+".svg", temp_dir+"/"+page_name);
		aux = dom.createElement("file");
		aux.setAttribute("href",page_name);
		pages_resource.appendChild(aux);
	
	#now create the groups for the test quizzes and we should be ready
	for i in range(1,4):
		qb_id = id_test_bank + i;
		href_attr = "page"+str(max_id + 2*i-1)+".svg";
		res = dom.createElement("resource");
		res.setAttribute("adlcp:scormType","asset");
		res.setAttribute("href",href_attr);
		res. setAttribute("identifier","group"+str(qb_id)+"_pages");
		res. setAttribute("type","webcontent");
		f1 = dom.createElement("file");
		f1.setAttribute("href","page"+str(max_id + 2*i-1)+".svg");
		f2 = dom.createElement("file");
		f2.setAttribute("href","page"+str(max_id + 2*i-0)+".svg");
		res.appendChild(f1);
		res.appendChild(f2);
		dom.getElementsByTagName("resources")[0].appendChild(res);
		
	#write back
	try:
		f = open(manifest_file, "w");
		f.truncate(0);
		dom.writexml(f);
		f.close();
	except IOError:
		raise Exception("Could not open file for writing");

#returns the absolute file name, base name and working directory
def get_file_path(input_file):
	f = open(input_file,"rb");
	abs_filename = os.path.abspath(f.name); #absolute filename, e.g. d:\work\quizzes\quiz1\Chapter1.notebook
	base_name = os.path.basename(f.name); #only the base name of the file, e.g. Chapter1.notebook
	working_dir = os.path.dirname(abs_filename);#directory of the file, e.g. d:\work\quizzes\quiz1
	f.close();
	
	return (abs_filename, base_name, working_dir);

#generate a file containing a range of answers from 1 to num_answers, all empty
def generate_answer_sheet(file_name, num_answers):
	if num_answers<1 or num_answers>500:
		raise ValueError();
	f = open(file_name, "wb");
	for i in range(1,num_answers+1):
		f.write(str(i)+") \r\n");
	f.close();

#returns the correct "current file" path
def get_current_path():
	exec_path = os.path.dirname(os.path.realpath(__file__));
		
	if not os.path.isdir(exec_path):
		#we're compiled, the result is the library.zip file, we need to do it again
		exec_path = os.path.dirname(exec_path);
	return exec_path;
