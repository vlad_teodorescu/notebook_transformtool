from Tkinter import *;
import tkFileDialog;
import Tkconstants;


#text contained in the about window
INFO_TEXT = """.notebook file processor
v 1.0
Licensed under GPL v3.0
See documentation for further details"""


#About Window
class AbootWindow(Toplevel):
	def __init__(self, master, cnf={}, **kw):
		
		Toplevel.__init__(self, master, cnf, **kw);
		self.info_label = Label(self, text=INFO_TEXT)
		self.info_label.grid(row=0, column=0, sticky = S);
		self.ok_button = Button(self, text="Ok", command=self.on_ok);
		self.ok_button.grid(row=1, column=0);
		self.grid_rowconfigure(0,pad = 20);
		self.grid_rowconfigure(1,pad = 20);
		self.grid_columnconfigure(0,pad = 20);
	def on_ok(self):
		self.destroy();
