import sys;
import zipfile;
import os.path;
import re;
import shutil;
import xml.dom.minidom;

#recusively archives a folder and its subfolders
def recursive_archive(z, path, basepath = None):
	file_list = os.listdir(path);
	if basepath is None:
		basepath = path;
	for i in file_list:
		if (i=="." or i==".."):
			continue;
		aux = path+"/"+i;
		if os.path.isdir(aux):
			recursive_archive(z,aux, basepath);
		else:
			archpath = path.replace(basepath,"");
			archname = i;
			if len(archpath):
				archname = archpath+"/"+i;
			z.write(aux, archname);

			
def extract_notebook_archive(archive_file, target_dir):
	#TODO: catch the exceptions from rmtree and handle them accordingly
	shutil.rmtree(target_dir, ignore_errors = True);
	arch = zipfile.ZipFile(archive_file, "r");
	arch.extractall(target_dir);
	arch.close();
	

def compress_as_notebook_archive(archive_file, target_dir):		
	os.remove(archive_file);
	arch = zipfile.ZipFile(archive_file, "w", zipfile.ZIP_DEFLATED);
	recursive_archive(arch, target_dir);
	arch.close();
	shutil.rmtree(target_dir, ignore_errors = True);